module.exports = function(grunt){

    require('load-grunt-tasks')(grunt);

    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'build'
    };


    grunt.initConfig({
        myApp : appConfig,

        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }

        },
        cssmin:{
            compress: {
                src: "<%= myApp.dist %>/css/styles.css",
                dest: "%= myApp.dist %/css/styles.min.css"
            }
        },
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                "globals": {
                    "angular": true,
                    "console":true
                }
            },
            target: [
                'Gruntfile.js',
                '<%= myApp.app %>/app.js',
                '<%= myApp.app %>/**/*.js'
            ]
        },
        uglify:{
            compress:{
                src:[
                    '<%= myApp.app %>/app.js',
                    '<%= myApp.app %>/**/*.js'
                ],
                dest:"<%= myApp.dist %>/app.min.js"
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            annotate: {
                files: {
                    'app/ann.js': ['<%= myApp.app %>/ann.js']
                }
            }
        },
        connect:{
                options:{
                    port:'9000',
                    livereload:35729,
                    hostname:'localhost'
                },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect.static('app')
                        ];
                    }
                }
            }
        },
        watch:{
            scripts:{
                files:[
                    '<%= myApp.app %>/app.js',
                    '<%= myApp.app %>/**/*.js'
                ],
                tasks:"jshint"
            },
       //     styles:{
         //       files:[
         //           "<%= myApp.app %>/sass/styles.scss",
         //           "<%= myApp.app %>/sass/partials/*.scss"
          //      ],
        //        tasks: "compass"
    //        },
            livereload:{
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files:['build/css/styles.css',
                    '<%= myApp.app %>/app.js',
                    '<%= myApp.app %>/**/*.js'
                ]
            }
        },
        ftpush:{
            build:{
                auth:{
                    host:'176.114.0.30',
                    port:'21',
                    authKey:'key1'
                },
                src:'build',
                dest:'/www/fox-hands.dp.ua'
            }
        }
    });

    grunt.registerTask('push', ['ftpush']);
    grunt.registerTask('build', ['uglify', 'cssmin']);
    grunt.registerTask('default', ['connect:livereload','watch']);

};
