angular.module('myApp').directive('currency', function(){
    var directiveDefinitionObject = {
        restrict:'A',
        required:'ngModel',
        link: function($scope, $elem, $attrs, ngModel){
            function validCurrency(number){
                if(null !== number.match(/[0-9]{3,4},[0-9]{2}$/)){
                    ngModel.$setValidity('number', true);
                }else{
                    ngModel.$setValidity('number', false);
                }
                return number;
            }

            ngModel.$parsers.push(function(viewValue){
                return validCurrency(viewValue);
            });
            ngModel.$formatters.push(function(modelValue){
                return validCurrency(modelValue);
            });

            $scope.safeApply();
        }
    };
    return directiveDefinitionObject;
});