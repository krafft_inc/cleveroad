angular.module('myApp').directive('shadow',['messaging', 'events',
  function(messaging, events){

  var directiveDefinitionObject ={
    restrict:"E",
    replace: true,
    template: "<div class='shadow'></div>",
    link: function($scope, $elem ){

      var showShadow = function(){
        $elem.toggleClass('shadow-visible');
      };


      $scope.showShadow = messaging.subscribe(events.message._SHOW_BODY_SHADOW_,showShadow);


      $scope.$on('$destroy', function(){
        messaging.unsubscribe($scope.showShadow);
      });
      $scope.safeApply();
    }
  };
  return directiveDefinitionObject;
}]);
