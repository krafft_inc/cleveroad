angular.module('myApp').directive('modal', function(messaging, events, Product, $document){
    var directiveDefinitionObject = {
        restrict:'E',
        templateUrl:'directive/modal/modal.html',
        replace:true,
        link: function($scope, $elem, $attr){

            var showModal = function(product){

                $scope.product = product || false;

                if(product){
                    $scope.title = product.title;
                    $scope.price = product.price;
                    $scope.description = product.description;
                }
                $elem.toggleClass('visible');
            };
            var hideModal = function(){
                $elem.toggleClass('visible');
                messaging.publish(events.message._SHOW_BODY_SHADOW_);
            };

            $scope.hideModal = function(){
                messaging.publish(events.message._HIDE_BODY_MODAL_);
                messaging.publish(events.message._GET_PRODUCTS_);
                $scope.title = '';
                $scope.price = '';
                $scope.description = '';
            };
            $scope.editGood = function(){
                $scope.product.title = $scope.title;
                $scope.product.price = $scope.price;
                $scope.product.description = $scope.description;
                messaging.publish(events.message._EDIT_GOOD_, [$scope.product]);
            };

            $scope.createGood = function(){
                var good = new Product ();
                good.title = $scope.title;
                good.price = $scope.price;
                good.description = $scope.description;
               messaging.publish(events.message._CREATE_GOOD_, [good]);
                $scope.title = '';
                $scope.price = '';
                $scope.description = '';
            };

            $scope.showModal = messaging.subscribe(events.message._SHOW_BODY_MODAL_,showModal);
            $scope.showModal = messaging.subscribe(events.message._HIDE_BODY_MODAL_,hideModal);


            $scope.$on('$destroy', function(){
                messaging.unsubscribe($scope.showModal);
                messaging.unsubscribe($scope.hideModal);
            });
            $scope.safeApply();
        }
    };

    return directiveDefinitionObject;
});