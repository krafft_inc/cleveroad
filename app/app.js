angular.module('myApp', [
  'ngRoute',
  'ngAnimate',
  'LocalStorageModule'
]).
config( ['$routeProvider', function($routeProvider) {
  $routeProvider.when('/authorization',{
        controller: 'AuthorizationCtrl',
        templateUrl: 'templates/authorization/authorization.html'
      }).when('/index',{
          controller: 'IndexCtrl',
          templateUrl: 'templates/index/index.html'
      }).when('/settings',{
          controller: 'SettingsCtrl',
          templateUrl: 'templates/settings/settings.html'
      })
      .otherwise({redirectTo: '/authorization'});
}])
    .config( function(localStorageServiceProvider){
        localStorageServiceProvider
            .setPrefix('myApp')
            .setStorageCookie(30,'/')
            .setStorageType('sessionStorage');
    });

angular.module('myApp').run(['$rootScope', function ($rootScope) {
    $rootScope.safeApply = function (fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);
angular.module('myApp').run(['mongolab', function(mongolab) {
    mongolab.setApiKey('OKLros-dRCKm1qnUlvlVlbVBV4JQb4mM');
    mongolab.setBaseUrl('https://api.mongolab.com/api/1/databases');
}]);
angular.module('myApp').run(['authenticate', function(authenticate){
    authenticate.init();
}]);
angular.module('myApp').run(['userDataService', function(userDataService){
    userDataService.init();
}]);
angular.module('myApp').run(['productDataService', function (productDataService){
    productDataService.init();
}]);
angular.module('myApp').run(['localStorageService', '$location', function(localStorageService, $location){
    if(localStorageService.get('user')){
        $location.path('/index');
    }
}]);
angular.module('myApp').controller('MainCtrl',['$scope','events','messaging','$location','$timeout', '$rootScope','localStorageService',
    function($scope, events, messaging,$location, $timeout, $rootScope, localStorageService){

    $scope.messagingHandles = [];

    $scope.subscribe = function (topic, callback) {
        var handle = messaging.subscribe(topic, callback);

        if (handle) {
            $scope.messagingHandles.push(handle);
        }
    };

    $scope.publish = function (topic, data) {
        messaging.publish(topic, data);
    };

    $scope.$on('$destroy', function () {
        angular.forEach($scope.messagingHandles, function (handle) {
            messaging.unsubscribe(handle);
        });
    });

    $scope.debounce = function(func, ms){
        var state = false;
        return function(){
            if(state){return;}
            func.apply(this, arguments);
            state = !state;
            $timeout(function(){
                state = false;
            }, ms);
        };
    };
        if(localStorageService.get('user')) {
            $scope.user = localStorageService.get('user');
        }
    $scope.onComplete = function(user){
            var data ={};
            angular.extend(data, user);

            if($scope.rememberMe){
                localStorageService.set('user', data);
            }

            $location.path('/index');
      //  $scope.unsubscribe($scope.onComplete);
    };

    $scope.subscribe(events.message._AUTHENTICATE_USER_COMPLETE_, $scope.onComplete);

}]);
