angular.module('myApp').controller('SettingsCtrl',['$scope','$controller','$location','localStorageService','events','mongolab', 'md5',
    function($scope, $controller, $location, localStorageService,events, mongolab, md5){

   $controller('MainCtrl', {$scope:$scope});
        $scope.user = localStorageService.get('user');
    $scope.email ='';
    $scope.onMain = function(){
        $location.path('/index');
    };
        $scope.newpassword = '';
        $scope.confirmnewpassword = '';

        $scope.edit = function(){
            console.log('request');
            return mongolab.queryById('cleveroad','users',{Email:$scope.user.Email})
                .then(getUserByEmailSuccessHandler,getUserByEmailErrorHandler);
        };
       var getUserByEmailSuccessHandler = function  (user){
           if(!user){
               getUserByEmailErrorHandler();
           }
           $scope.user = user.data[0];
           $scope.user.firstname = $scope.firstname || $scope.user.firstname;
           $scope.user.secondname = $scope.secondname || $scope.user.secondname;
           $scope.user.Email = $scope.email || $scope.user.Email;
           $scope.user.Phone = $scope.phone || $scope.user.Phone;

           $scope.user.Password = md5.createHash('pepper'+$scope.newpassword+Date.parse($scope.user.DateJoined.valueOf().toString())+'salt')

           $scope.publish(events.message._UPDATE_USER_INFO_, [$scope.user]);
           console.log('final');
       };
        var getUserByEmailErrorHandler = function(){
            console.log('error');
        };

}]);