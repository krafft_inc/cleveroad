angular.module('myApp').controller('IndexCtrl', ['$scope','$controller','events','messaging','$location','$rootScope','localStorageService',
    function($scope, $controller, events, messaging, $location, $rootScope, localStorageService){
      $controller('MainCtrl', {$scope:$scope});




        if(!$scope.user && !localStorageService.get('user')){
            $location.path('/authorization');
        }else{
            $scope.user = localStorageService.get('user');
            console.log($scope.user._id);
        }


        messaging.publish(events.message._GET_PRODUCTS_);


        var renderProducts = function(result){
          $scope.products = result;
            console.log($scope.products);
        };
        messaging.subscribe(events.message._GET_PRODUCTS_COMPLETE_,renderProducts);

        $scope.addGood = function(product){
            messaging.publish(events.message._SHOW_BODY_SHADOW_);
            messaging.publish(events.message._SHOW_BODY_MODAL_, [product]);
        };

        $scope.showDescription = function(index){
            angular.forEach($scope.products, function(val){
               if(val.showInfo){
                   delete val.showInfo;
               }
            });
            $scope.products[index].showInfo = true;
        };
        $scope.hideDescription = function(index){
           delete $scope.products[index].showInfo;
        };

        $scope.editGood = function(index){
            $scope.addGood($scope.products[index]);
        };

        $scope.deleteGood = function(index){
            messaging.publish(events.message._DELETE_GOOD_,[$scope.products[index]]);
        };
        $scope.deleteFew = function(){
            angular.forEach($scope.products, function(val){
                if(val.delete){
                    messaging.publish(events.message._DELETE_GOOD_,[val]);
                }
            });
        };
        $scope.goToSettings = function(){
            $location.path('/settings');
        };
        $scope.logout = function(){
         localStorageService.remove('user');
            $scope.user = '';
            $location.path('/authorization');
        };
}]);