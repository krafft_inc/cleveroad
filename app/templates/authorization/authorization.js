angular.module('myApp').controller('AuthorizationCtrl',
    function($scope,  $controller, events, messaging, User, $location, sha, md5){
    $controller('MainCtrl', {$scope:$scope});


        $scope.rememberMe = true;


    $scope.signUp = function(event){
        $scope.registration = false;
    };
    $scope.registrate = function(){
        $scope.registration = true;
    };

    $scope.user = new User();
    $scope.email = '';
    $scope.password='';
    $scope.confirmpassword='';
    $scope.currentUser = '';
    $scope.currentPassword='';


    $scope.registerUser = function(){
        $scope.user.Email = $scope.email;

        $scope.user.DateJoined = new Date();
        $scope.user.Password = md5.createHash('pepper'+$scope.password+$scope.user.DateJoined.valueOf().toString()+'salt');

        $scope.publish(events.message._CREATE_USER_, [$scope.user]);

        $scope.email = '';
        $scope.password = '';
        $scope.confirmpassword = '';
        $scope.registration = false;
    };
    $scope.authenticate = function(){
        $scope.publish(events.message._AUTHENTICATE_USER_BY_EMAIL_,[{Email:$scope.currentUser}, $scope.currentPassword]);
    };

});