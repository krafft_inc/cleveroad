angular.module('myApp').factory('userDataService',function(messaging, events, mongolab,modelTransformer,User){
var users = [];
var currentUser = {};


  var getCurrentUser = function(){
    return currentUser;

  };
  var getUserByEmail = function(email){

    return mongolab.queryById('cleveroad','users',email,[])
      .then(getUserByEmailSuccessHandler,getUserByEmailErrorHandler);
  };
  messaging.subscribe(events.message._GET_USER_BY_EMAIL_,getUserByEmail);

  var getUserByEmailSuccessHandler = function(response){
    if(response.data.length >0){
      var result = [];
      angular.forEach(response.data,function(user){
        result.push(modelTransformer.transform(user, User));
      });
      messaging.publish(events.message._GET_USER_BY_EMAIL_COMPLETE_,[result]);
      console.log('getUserSuccessComplete');
      currentUser = result;
    }else{
      getUserByEmailErrorHandler();
    }
  };

  var getUserByEmailErrorHandler = function(){
    console.log('getUserFailureOccurs');
  };


  var createUser = function (user) {

    return mongolab.create('cleveroad', 'users', user)
      .then(createUserSuccessHandler, createUserErrorHandler);
  };

  var createUserSuccessHandler = function (response) {
    if (response.data) {
      messaging.publish(events.message._CREATE_USER_COMPLETE_,
        [modelTransformer.transform(response.data, User)]);
    } else {
      createUserErrorHandler();
    }
  };

  var createUserErrorHandler = function () {
    console.log('createUserFailureOccurs');
  };

  messaging.subscribe(events.message._CREATE_USER_, createUser);

var updateUser = function(user){
  return mongolab.update('cleveroad','users', user).
      then(updateUserSuccessHandler, updateUserErrorHandler);
};
  messaging.subscribe(events.message._UPDATE_USER_INFO_, updateUser);

var  updateUserSuccessHandler = function (){
  console.log('user was upated');
};
  var updateUserErrorHandler = function(){
    console.log('user wasn\'t updated');
  };
  var init = function () {
    users = [];
  };

 var userDataService={
   getCurrentUser:getCurrentUser,
   init:init,
   getUserByEmail:getUserByEmail,
   getUserByEmailSuccessHandler:getUserByEmailSuccessHandler,
   getUserByEmailErrorHandler:getUserByEmailErrorHandler,
   createUser:createUser,
   createUserSuccessHandler:createUserSuccessHandler,
   createUserErrorHandler:createUserErrorHandler
 };
  return userDataService;
});

