angular.module('myApp').service('productDataService',
  function(messaging, events, mongolab, modelTransformer, Product){
  var products = [];

  var getProducts = function(){

    console.log('getProducts');
    return mongolab.query('cleveroad','goods',[])
      .then(getProductsSuccessHandler,getProductsErrorHandler);
  };

  var getProductsSuccessHandler = function(response){
    if(response.data.length >0){
      var result = [];
      angular.forEach(response.data, function(product){
        result.push(modelTransformer.transform(product, Product));
      });
      messaging.publish(events.message._GET_PRODUCTS_COMPLETE_,[result]);
    }else{
      getProductsErrorHandler();
    }
  };

  var getProductsErrorHandler = function() {
    console.log('getProductsFailure');
  };

  messaging.subscribe(events.message._GET_PRODUCTS_,getProducts);

  var getProductById = function(id){
    return mongolab.queryById('cleveroad','goods',id,[])
      .then(getProductByIdSuccessHandler,getProductByIdErrorHandler);
  };



  var getProductByIdSuccessHandler = function(response){
    if(response.data.length >0){

    var result = [];
    angular.forEach(response.data,function(product){
      result.push(modelTransformer.transform(product,Product));
      console.log(result);
    });
      messaging.publish(events.message._GET_PRODUCT_BY_ID_COMPLETE_,[result]);
    }else{
      getProductByIdErrorHandler();
    }
  };

  var getProductByIdErrorHandler = function(){
    console.log('getProductsFailure');
  };
     messaging.subscribe(events.message._GET_PRODUCT_BY_ID_,getProductById);


  var createProduct = function(product){

    return mongolab.create('cleveroad', 'goods', product)
        .then(createProductSuccessHandler, createProductErrorHandler);
  };
  var createProductSuccessHandler = function(){
    console.log('DONE');
  };
  var createProductErrorHandler = function (){
    console.log('createProductError');
  };
    messaging.subscribe(events.message._CREATE_GOOD_,createProduct);

  var deleteProduct = function(product){
    return mongolab.delete('cleveroad', 'goods', product)
        .then(deleteProductSuccessHandler, deleteProductErrorHandler);
  };
  var deleteProductSuccessHandler = function(){
    messaging.publish(events.message._GET_PRODUCTS_);

  };
    var deleteProductErrorHandler = function(){
      console.log('deleteError');
    };
messaging.subscribe(events.message._DELETE_GOOD_, deleteProduct);

    var editProduct = function(product) {
      return mongolab.update('cleveroad', 'goods', product).
          then(editProductSuccessHandler, editProductErrorHandler);
    };
    messaging.subscribe(events.message._EDIT_GOOD_,editProduct );

    var  editProductSuccessHandler = function(){
      console.log('product was edited');
    };
    var editProductErrorHandler = function(){
      console.log('error product wasn\'t edite');
    };


  var init = function(){
    products = [];
  };

  var productDataService ={
    init:init,
    getProducts:getProducts,
    getProductsSuccessHandler:getProductsSuccessHandler,
    getProductsErrorHandler:getProductsErrorHandler,
    getProductById:getProductById,
    getProductByIdSuccessHandler:getProductByIdSuccessHandler,
    getProductByIdErrorHandler:getProductByIdErrorHandler,
    createProduct:createProduct,
    createProductSuccessHandler:createProductSuccessHandler,
    createProductErrorHandler:createProductErrorHandler,
    deleteProduct:deleteProduct,
    deleteProductSuccessHandler:deleteProductSuccessHandler,
    deleteProductErrorHandler:deleteProductErrorHandler,
    editProduct:editProduct,
    editProductSuccessHandler:editProductSuccessHandler,
    editProductErrorHandler:editProductErrorHandler
  };

  return productDataService;
 });
